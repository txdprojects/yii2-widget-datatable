<?php

namespace txd\widgets\datatable;

use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * FieldColumn displays a input field for each row of the table.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class FieldColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $className = 'field-column col-autowidth';


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}
}
