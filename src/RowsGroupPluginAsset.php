<?php

namespace txd\widgets\datatable;

use yii\web\AssetBundle;

/**
 * RowsGroupPluginAsset registers DataTable specific plugin asset bundle.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class RowsGroupPluginAsset extends AssetBundle
{
	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/dataTables.rowsGroup.js',
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		'\txd\widgets\datatable\DataTableAsset',
	];

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';
	}
}
