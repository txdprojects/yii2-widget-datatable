<?php

namespace txd\widgets\datatable;

use yii\helpers\Html;

/**
 * SerialColumn displays a media item for each row of the table.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class MediaColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $className = 'media-column col-autowidth text-center';

	/**
	 * {@inheritdoc}
	 */
	public $searchable = false;

	/**
	 * {@inheritdoc}
	 */
	public $orderable = false;

	/**
	 * {@inheritdoc}
	 */
	public $filter = false;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}
}
