$.extend(true, $.fn.dataTable.defaults.oLanguage, {
	"oAria": {
		"sSortAscending": ": activează pentru a sorta coloana crescător",
		"sSortDescending": ": activează pentru a sorta coloana descrescător"
	},
	"oPaginate": {
		"sFirst": "Prima",
		"sLast": "Ultima",
		"sNext": "Următoarea",
		"sPrevious": "Precedenta"
	},
	"select": {
		"rows": {
			"_": "%d rânduri selectate",
			"0": "",
			"1": "%d rând selectat"
		}
	},
	"sDecimal": "",
	"sEmptyTable": "Nu există date în tabel.",
	"sInfo": "Afișate de la _START_ la _END_ din _TOTAL_ înregistrări",
	"sInfoEmpty": "Afișate de la 0 la 0 din 0 înregistrări",
	"sInfoFiltered": "(filtrate dintr-un total de _MAX_ înregistrări)",
	"sInfoPostFix": "",
	"sLengthMenu": "Afișează _MENU_ înregistrări pe pagină",
	"sLoadingRecords": "Încărcare...",
	"sProcessing": "Procesare...",
	"sSearch": "Caută:",
	"sSearchPlaceholder": "",
	"sThousands": ",",
	"sZeroRecords": "Nu au fost găsite date."
});