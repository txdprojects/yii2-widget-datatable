<?php

namespace txd\widgets\datatable;

use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * NumericColumn is used to format numeric data for table cells.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class NumericColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $className = 'numeric-column col-autowidth';


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}
}
