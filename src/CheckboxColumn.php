<?php

namespace txd\widgets\datatable;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * SerialColumn displays a checkbox each row of the table.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class CheckboxColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $className = 'checkbox-column col-autowidth text-center';

	/**
	 * {@inheritdoc}
	 */
	public $searchable = false;

	/**
	 * {@inheritdoc}
	 */
	public $orderable = false;

	/**
	 * @var array Checkbox configuration.
	 */
	public $checkbox = [
		'name' => 'selection[]',
		'valueAttribute' => 'id',
	];


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->title = strtr($this->getCheckbox(1, '_all'), [
			'__id__' => 'dt-selection_all-' . rand(0, 9999),
		]);
		$this->render = $this->buildRender();
	}

	/**
	 * Gets a custom checkbox HTML tag.
	 *
	 * @param int|string $value
	 * @param int|string $suffix
	 * @return string
	 */
	protected function getCheckbox($value, $suffix = null)
	{
		$checkboxOptions = array_merge([
			'id' => '__id__',
			'class' => 'custom-control-input',
			'type' => 'checkbox',
			'value' => $value,
			'labelOptions' => [
				'class' => 'custom-control-label',
				'for' => '__id__',
			],
		], $this->checkbox);
		ArrayHelper::remove($checkboxOptions, 'valueAttribute');

		// Get the checkbox name from configuration
		$name = ArrayHelper::remove($checkboxOptions, 'name');

		// Append the suffix for checkbox name
		if (!is_null($suffix)) {
			// Get the name as array parts
			$parts = array_filter(explode('|', preg_replace('/\[|\]/', '|', $name)));
			if (count($parts) === 1) {
				$name = $parts[0] . $suffix;
			} else {
				$name = '';
				foreach ($parts as $key => $part) {
					if ($key == 0) {
						$name .= $part;
					} elseif ($key == 1) {
						$name .= '[' . ($part . $suffix) . ']';
					} else {
						$name .= "[{$part}]";
					}
				}
			}
		}
		$checkboxOptions['name'] = $name;
		$labelOptions = ArrayHelper::remove($checkboxOptions, 'labelOptions', []);

		return Html::tag('div', implode('', [
			Html::tag('input', null, $checkboxOptions),
			Html::tag('label', false, $labelOptions),
		]), [
			'class' => 'custom-control custom-checkbox',
		]);
	}

	/**
	 * Builds a custom renderer.
	 *
	 * @return string|JsExpression
	 */
	protected function buildRender()
	{
		if (!empty($this->render)) {
			return $this->render;
		}

		return new JsExpression("function (data, type, row, meta) {
			return '" . $this->getCheckbox('__value__') . "'
				.replace(/__id__/gmi, meta.settings.sInstance + '-checkbox-column-' + meta.row)
				.replace(/__value__/gmi, row['" . $this->checkbox['valueAttribute'] . "']);
		}");
	}
}
