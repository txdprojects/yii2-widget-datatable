<?php

namespace txd\widgets\datatable;

use Yii;
use yii\web\JsExpression;

/**
 * BaseDataTableColumn serves as a base class for all table column types.
 *
 * @link https://datatables.net/reference/option/columns
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class BaseDataTableColumn extends \yii\base\BaseObject implements \ArrayAccess
{
	/**
	 * @var string Cell type to be created for a column.
	 */
	public $cellType = 'td';

	/**
	 * @var string CSS Class to assign to each cell in the column.
	 */
	public $className;

	/**
	 * @var string Set the data source for the column from the rows data object / array.
	 */
	public $data;

	/**
	 * @var string Set a descriptive name for a column.
	 */
	public $name;

	/**
	 * @var string Set default, static, content for a column.
	 */
	public $defaultContent;

	/**
	 * @var bool Enable or disable ordering on this column.
	 */
	public $orderable = true;

	/**
	 * @var bool Enable or disable filtering on the data in this column.
	 */
	public $searchable = true;

	/**
	 * @var string Render (process) the data for use in the table.
	 */
	public $render;

	/**
	 * @var string Set the column title.
	 */
	public $title;

	/**
	 * @var string Set the column type - used for filtering and sorting string processing.
	 */
	public $type;

	/**
	 * @var bool Enable or disable the display of this column.
	 */
	public $visible = true;

	/**
	 * @var string|int|double Column width assignment.
	 */
	public $width;

	/**
	 * @var string|null Cell created callback to allow DOM manipulation.
	 */
	public $createdCell = null;

	// Custom
	/**
	 * @var string|array Column filter content.
	 */
	public $filter;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		if (isset($this->render)) {
			if (!($this->render instanceof JsExpression)) {
				$this->render = new JsExpression($this->render);
			}
		}
	}

	//region ArrayAccess
	/**
	 * {@inheritdoc}
	 */
	public function offsetExists($offset)
	{
		return isset($this->$offset);
	}

	/**
	 * {@inheritdoc}
	 */
	public function offsetGet($offset)
	{
		return $this->$offset;
	}

	/**
	 * {@inheritdoc}
	 */
	public function offsetSet($offset, $value)
	{
		$this->$offset = $value;
	}

	/**
	 * {@inheritdoc}
	 */
	public function offsetUnset($offset)
	{
		$this->$offset = null;
	}
	//endregion ArrayAccess
}
