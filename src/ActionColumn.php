<?php

namespace txd\widgets\datatable;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * ActionColumn displays action controls for each row of the table.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class ActionColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $className = 'action-column col-autowidth';

	/**
	 * {@inheritdoc}
	 */
	public $orderable = false;

	/**
	 * {@inheritdoc}
	 */
	public $searchable = false;

	/**
	 * @var array The action controls list in action column.
	 */
	public $buttons = [];

	/**
	 * @var int The number of buttons per row.
	 */
	public $buttonsPerRow = 5;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->initDefaults();

		if (empty($this->data)) {
			$this->render = $this->buildRender();
		}
	}

	/**
	 * Initializes default attributes.
	 */
	protected function initDefaults()
	{
		// Filter the buttons
		$this->buttons = array_filter($this->buttons, function ($item) {
			return $item['visible'] === true;
		});
		$this->buttons = ArrayHelper::getColumn($this->buttons, 'content');

		// Center single button in the column
		if (count($this->buttons) === 1) {
			$this->className .= ' text-center';
		}

		// Hide this column if there are no buttons to display
		if (empty($this->data) && empty($this->buttons)) {
			$this->visible = false;
		}
	}

	/**
	 * Builds a custom renderer.
	 *
	 * @return string|JsExpression
	 */
	protected function buildRender()
	{
		if (!empty($this->render)) {
			return $this->render;
		}

		return new JsExpression("function (data, type, row, meta) {
			var buttons = " . Json::encode($this->buttons) . ";
			var buttonsPerRow = +" . $this->buttonsPerRow . ";
			var columnButtons = ['<div>'];
			var i = 1;

			$.each(buttons, function (name, button) {
				columnButtons.push(button.replace(/(__\w+__)/gmi, function (match, tag, char) {
					var param = match.replace(/__/gmi, '');
					return row[param];
				}));

				if (i % buttonsPerRow === 0) {
					columnButtons.push('</div><div>');
				}
				i++;
			});
			columnButtons.push('</div>');

			return columnButtons.join('');
		}");
	}
}
