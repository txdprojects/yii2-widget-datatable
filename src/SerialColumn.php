<?php

namespace txd\widgets\datatable;

use yii\web\JsExpression;

/**
 * SerialColumn displays a numeric index for each row of the table.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class SerialColumn extends BaseDataTableColumn
{
	/**
	 * {@inheritdoc}
	 */
	public $title = '#';

	/**
	 * {@inheritdoc}
	 */
	public $className = 'serial-column col-autowidth text-center';

	/**
	 * {@inheritdoc}
	 */
	public $searchable = false;

	/**
	 * {@inheritdoc}
	 */
	public $orderable = false;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->render = $this->buildRender();
	}

	/**
	 * Builds a custom renderer.
	 *
	 * @return JsExpression
	 */
	protected function buildRender()
	{
		return new JsExpression("function (data, type, row, meta) {
			return (meta.row + 1);
		}");
	}
}
