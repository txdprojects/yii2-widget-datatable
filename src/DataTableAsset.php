<?php

namespace txd\widgets\datatable;

use Yii;
use yii\web\AssetBundle;

/**
 * DataTableAsset registers DataTable asset bundle.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class DataTableAsset extends AssetBundle
{
	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/datatables.min.css',
		'css/yii.dataTable.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/datatables.min.js',
		'js/yii.dataTable.js',
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		'yii\web\JqueryAsset',
	];

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';

		// Publish DataTable language file if exists
		$language = substr(Yii::$app->language, 0, 2);
		if (file_exists( "{$this->sourcePath}/i18n/{$language}.js")) {
			$this->js[] = "i18n/{$language}.js";
		}
	}
}
