<?php

namespace txd\widgets\datatable;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\mongodb\ActiveQuery;
use yii\mongodb\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Action class that process DataTable AJAX request with ActiveDataProvider for MongoDB.
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class DataTableMongoAction extends BaseDataTableAction
{
	/**
	 * @var \yii\mongodb\ActiveQuery The query used as dataset of the table.
	 */
	public $query;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * Runs this action.
	 *
	 * @return mixed
	 * @throws InvalidConfigException if query is not properly configured.
	 */
	public function run()
	{
		if (is_null($this->query)) {
			throw new InvalidConfigException(self::class . '::$query must be an instance of \yii\mongodb\ActiveQuery.');
		}

		$originalQuery = $this->query;
		$filteredQuery = clone $originalQuery;
		$filteredQuery->where = null;

		$draw = $this->getParam('draw');
		$search = $this->getParam('search', ['value' => null, 'regex' => false]);
		$columns = $this->getParam('columns', []);
		$order = $this->getParam('order', []);

		$filteredQuery
			->offset($this->getParam('start', 0))
			->limit($this->getParam('length', -1));

		$filteredQuery = $this->applyFilter($filteredQuery, $columns, $search);
		$filteredQuery = $this->applyOrder($filteredQuery, $columns, $order);
		if (!empty($originalQuery->where)) {
			$filteredQuery->andWhere($originalQuery->where);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $filteredQuery,
			'pagination' => [
				'pageSize' => $this->getParam('length', 10),
			],
		]);

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		try {
			if (!$this->beforeResponse($dataProvider)) {
				throw new \Exception('Sending the response was stopped.');
			}

			$response = [
				'draw' => (int) $draw,
				'recordsTotal' => (int) $originalQuery->count(),
				'recordsFiltered' => (int) $dataProvider->getTotalCount(),
				'data' => $this->formatData($filteredQuery, $columns),
			];
		} catch (\Exception $e) {
			return ['error' => $e->getMessage()];
		}

		return $this->formatResponse($response);
	}

	/**
	 * Hook before sending the response.
	 *
	 * @param ActiveDataProvider $dataProvider The data provider.
	 * @return bool whether to return the response.
	 */
	public function beforeResponse(ActiveDataProvider $dataProvider)
	{
		return true;
	}

	/**
	 * Applies filtering.
	 *
	 * @param \yii\mongodb\ActiveQuery $query The query used as dataset of the table.
	 * @param array $columns The table columns.
	 * @param array $search The table searchable columns.
	 * @return \yii\mongodb\ActiveQuery The filtered dataset.
	 * @throws InvalidConfigException
	 */
	public function applyFilter(ActiveQuery $query, $columns, $search)
	{
		if (is_callable($this->applyFilter)) {
			return call_user_func($this->applyFilter, $query, $columns, $search);
		}

		/** @var \yii\mongodb\ActiveRecord $modelClass */
		$modelClass = $query->modelClass;
		$modelAttributes = $modelClass::instance()->attributes();

		foreach ($columns as $column) {
			if ($column['searchable'] == 'true' && in_array($column['data'], $modelAttributes) !== false) {
				if (!empty($search['value'])) {
					$value = $search['value'];
					$filterOperator = 'orFilterWhere';
				} else {
					$value = $column['search']['value'];
					$filterOperator = 'andFilterWhere';
				}
				$query->$filterOperator(['like', $column['data'], $value]);
			}
		}

		return $query;
	}

	/**
	 * Applies ordering.
	 *
	 * @param \yii\mongodb\ActiveQuery $query The query used as dataset of the table.
	 * @param array $columns The table columns.
	 * @param array $order The table sortable columns.
	 * @return \yii\mongodb\ActiveQuery The ordered dataset.
	 */
	public function applyOrder(ActiveQuery $query, $columns, $order)
	{
		if (is_callable($this->applyOrder)) {
			return call_user_func($this->applyOrder, $query, $columns, $order);
		}

		foreach ($order as $key => $item) {
			$column = $columns[$item['column']];
			if (array_key_exists('orderable', $column) && $column['orderable'] == 'false') {
				continue;
			}
			$sort = mb_strtolower($item['dir']) == 'desc' ? SORT_DESC : SORT_ASC;
			$query->addOrderBy([$column['data'] => $sort]);
		}

		return $query;
	}

	/**
	 * Formats the dataset.
	 *
	 * @param \yii\mongodb\ActiveQuery $query The query used as dataset of the table.
	 * @param array $columns The table columns.
	 * @return array|ActiveRecord[] The formatted dataset.
	 */
	public function formatData(ActiveQuery $query, $columns)
	{
		if (is_callable($this->formatData)) {
			return call_user_func($this->formatData, $query, $columns);
		}

		return $query->all();
	}

	/**
	 * Formats the response.
	 *
	 * @param array $response
	 * @return array|ActiveRecord[] The formatted response.
	 */
	public function formatResponse($response)
	{
		if (is_callable($this->formatResponse)) {
			return call_user_func($this->formatResponse, $response);
		}

		return $response;
	}
}
