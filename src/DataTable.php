<?php

namespace txd\widgets\datatable;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * Class DataTable.
 *
 * @link https://datatables.net/
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class DataTable extends Widget
{
	/**
	 * @var bool|string Flag that indicates if a placeholder should replace the table until it has data to show.
	 * Can take the following types of values:
	 * - a false value: meaning this is disabled, an empty table will be shown
	 * - a true value: will search for any DOM elements with [data-dt-empty-placeholder="TABLE_ID"] attribute
	 * - a view path: it will render the view before the table (eg. @path/to/view/file.php)
	 * - a plain string: it will render the plain string before the table
	 */
	public $showEmptyPlaceholder = false;

	/**
	 * @var bool Flag that indicates if column filters should be visible.
	 */
	public $showColumnFilters = true;

	/**
	 * @var array Array with DataTable cached configuration.
	 */
	public $cacheData = [];

	/**
	 * @var array The widget options.
	 */
	public $options = [];

	/**
	 * @var array The client (JS) options.
	 */
	public $clientOptions = [];

	/**
	 * @var array The client (JS) events.
	 */
	public $clientEvents = [];

	/**
	 * @var string The client (JS) selector.
	 */
	private $_clientSelector;

	/**
	 * @var string The global widget JS hash variable.
	 */
	private $_hashVar;

	/**
	 * @var array A collection of column filters.
	 */
	private $_columnFilters = [];


	/**
	 * {@inheritdoc}
	 * @throws InvalidConfigException
	 */
	public function init()
	{
		parent::init();

		$this->setupProperties();
		$this->setupColumns();
		$this->registerAssets();

		// Begin widget content
		ob_start();
	}

	/**
	 * {@inheritdoc}
	 */
	public function run()
	{
		// End widget content - Get the HTML as the widget content
		$content = ob_get_clean();

		// Create the empty table placeholder
		$emptyPlaceholder = '';

		if ($this->showEmptyPlaceholder !== false) {
			Html::addCssClass($this->options, 'dt-empty-placeholder');

			if (is_string($this->showEmptyPlaceholder)) {
				$firstChar = mb_substr($this->showEmptyPlaceholder, 0, 1);
				if ($firstChar === '@' && is_file(Yii::getAlias($this->showEmptyPlaceholder))) {
					// Render the placeholder view
					$emptyPlaceholder = Html::tag('div', $this->renderFile($this->showEmptyPlaceholder), ['data-dt-empty-placeholder' => $this->getId()]);
				} elseif (!in_array($firstChar, ['#', '.', '['])) {
					// Render the plain string
					$emptyPlaceholder = Html::tag('div', $this->showEmptyPlaceholder, ['data-dt-empty-placeholder' => $this->getId()]);
				}
			}
		}

		return $emptyPlaceholder . Html::tag('table', $content, $this->options);
	}

	/**
	 * Gets the client selector.
	 *
	 * @return string
	 */
	public function getClientSelector()
	{
		if (!$this->_clientSelector) {
			$this->_clientSelector = '#' . $this->getId();
		}
		return $this->_clientSelector;
	}

	/**
	 * Gets the hash variable.
	 *
	 * @return string
	 */
	public function getHashVar()
	{
		if (!$this->_hashVar) {
			$this->_hashVar = 'datatable_' . hash('crc32', $this->buildClientOptions());
		}
		return $this->_hashVar;
	}

	/**
	 * Sets the widget properties.
	 */
	protected function setupProperties()
	{
		if ($this->options['id']) {
			$this->setId($this->options['id']);
		} else {
			$this->options['id'] = $this->getId();
		}

		Html::addCssClass($this->options, 'data-table');
	}

	/**
	 * Builds the client options.
	 *
	 * @return string
	 */
	protected function buildClientOptions()
	{
		$defaultClientOptions = [
			// Defaults can be added here
		];

		// Setup cached data
		if (is_array($this->cacheData) && !empty($this->cacheData)) {
			if (is_array($this->cacheData['order']) && !empty($this->cacheData['order'])) {
				$this->clientOptions['order'] = array_map(function ($order) {
					return [(int) $order['column'], $order['dir']];
				}, $this->cacheData['order']);
			}
			if (is_array($this->cacheData['columns']) && !empty($this->cacheData['columns'])) {
				$this->clientOptions['searchCols'] = array_map(function ($column) {
					return !empty($column['search']['value']) ? ['search' => $column['search']['value']] : null;
				}, $this->cacheData['columns']);
			}
			if (is_array($this->cacheData['search']) && isset($this->cacheData['search']['value'])) {
				$this->clientOptions['search'] = ['search' => $this->cacheData['search']['value']];
			}
			if (isset($this->cacheData['start'])) {
				$this->clientOptions['displayStart'] = (int) $this->cacheData['start'];
			}
			if (isset($this->cacheData['length'])) {
				$this->clientOptions['pageLength'] = (int) $this->cacheData['length'];
			}
		}

		// Build the order
		if (!empty($this->clientOptions['order'])) {
			$this->clientOptions['order'] = $this->buildOrder();
		}

		// Ensure that the pageLength always has an integer value and setup a fallback value
		$this->clientOptions['pageLength'] = (int) $this->clientOptions['pageLength'];
		if ($this->clientOptions['pageLength'] == 0) {
			$this->clientOptions['pageLength'] = 25;
		}

		// Build the lengthMenu
		if (is_array($this->clientOptions['lengthMenu']) && $this->clientOptions['lengthMenu']['autoCreate'] === true) {
			$this->clientOptions['lengthMenu'] = self::buildLengthMenu($this->clientOptions['pageLength'], $this->clientOptions['lengthMenu']['displayAll']);
		}

		// Check for table empty placeholder
		if ($this->showEmptyPlaceholder !== false) {
			if ($this->showEmptyPlaceholder === true) {
				// Instruct JS script to display any content with [data-dt-empty-placeholder="TABLE_ID"] attribute
				$this->clientOptions['showEmptyPlaceholder'] = true;
			} elseif (is_string($this->showEmptyPlaceholder)) {
				$firstChar = mb_substr($this->showEmptyPlaceholder, 0, 1);

				if ($firstChar === '@' && is_file(Yii::getAlias($this->showEmptyPlaceholder))) {
					// Check for a valid view path
					$this->clientOptions['showEmptyPlaceholder'] = true;
				} elseif (in_array($firstChar, ['#', '.', '['])) {
					// Check for a DOM selector
					$this->clientOptions['showEmptyPlaceholder'] = $this->showEmptyPlaceholder;
				} else {
					// Check for a plain string
					$this->clientOptions['showEmptyPlaceholder'] = true;
				}
			}
		}

		return Json::encode(ArrayHelper::merge($defaultClientOptions, $this->clientOptions));
	}

	/**
	 * Registers the widget assets.
	 */
	protected function registerAssets()
	{
		// Get the view
		$view = $this->getView();

		// Register assets
		DataTableAsset::register($view);
		if (!empty($this->clientOptions['rowsGroup'])) {
			RowsGroupPluginAsset::register($view);
		}
		// Register widget hash JavaScript variable
//		$view->registerJs("var {$this->getHashVar()} = {$this->buildClientOptions()};", View::POS_HEAD);

		// Register DataTable inline JavaScript for rendering column filters row
		if (!empty($this->_columnFilters)) {
			// Append the filters row based on the scrollX/Y property
			if ($this->clientOptions['scrollX'] || $this->clientOptions['scrollY']) {
				$view->registerJs("jQuery('{$this->getClientSelector()}').on('init.dt', function (e) {
					$(e.target).closest('.dataTables_scroll').find('.dataTables_scrollHead thead').append({$this->buildColumnFiltersRow()});
				});");
			} else {
				$view->registerJs("jQuery('{$this->getClientSelector()}').on('init.dt', function (e) {
					$(e.target).children('thead').append({$this->buildColumnFiltersRow()});
				});");
			}
		}

		// Build client script
		$js = "jQuery('{$this->getClientSelector()}').yiiDataTable({$this->buildClientOptions()})";

		// Build client events
		if (!empty($this->clientEvents)) {
			foreach ($this->clientEvents as $clientEvent => $eventHandler) {
				if (!($eventHandler instanceof JsExpression)) {
					$eventHandler = new JsExpression($eventHandler);
				}
				$js .= ".on('{$clientEvent}', {$eventHandler})";
			}
		}

		// Register widget JavaScript
		$view->registerJs("{$js};");
	}

	/**
	 * Sets the DataTable columns.
	 *
	 * @throws InvalidConfigException if column configuration is invalid.
	 */
	protected function setupColumns()
	{
		if (!isset($this->clientOptions['columns'])) {
			return;
		}

		foreach ($this->clientOptions['columns'] as $i => $column) {
			if (is_string($column)) {
				$this->clientOptions['columns'][$i] = [
					'data' => $column,
					'name' => $column,
				];
			} elseif (is_array($column)) {
				if (!isset($column['class'])) {
					$column['class'] = BaseDataTableColumn::class;
				}
				if (!isset($column['name'])) {
					$column['name'] = $column['data'];
				}

				$columnObject = Yii::createObject($column);

				if ($columnObject->visible === false) {
					unset($this->clientOptions['columns'][$i]);
					$this->updateNumericOrder($i);
					continue;
				}
				if ($this->showColumnFilters === true) {
					$this->_columnFilters[] = $columnObject->filter;
				}

				$this->clientOptions['columns'][$i] = $this->filterColumnAttributes($columnObject);

				if ($column['class'] === CheckboxColumn::class && !$this->clientOptions['select']) {
					$this->clientOptions['select'] = [
						'style' => 'multi',
						'selector' => '.checkbox-column :checkbox',
					];
				}
			}
		}

		// Reindex final columns
		$this->clientOptions['columns'] = array_values($this->clientOptions['columns']);
	}

	/**
	 * Updates order property starting from an index.
	 * Subtracts one for numeric ordering rule.
	 *
	 * @param int $index The start index of columns.
	 */
	protected function updateNumericOrder($index)
	{
		if (!empty($this->clientOptions['order'])) {
			foreach ($this->clientOptions['order'] as &$order) {
				if (is_numeric($order[0]) && $order[0] >= $index) {
					$order[0] -= 1;
				}
			}
		}
	}

	/**
	 * Build the order client option.
	 * This method maps the column name to its index if is the case.
	 *
	 * @return array Sortable columns mapped from column name to column index.
	 */
	protected function buildOrder()
	{
		$order = [];

		foreach ($this->clientOptions['order'] as $sortOrder) {
			if (!is_numeric($sortOrder[0])) {
				$sortOrder[0] = $this->getColumnIndexByName($sortOrder[0]);
			}
			$order[] = $sortOrder;
		}

		return $order;
	}

	/**
	 * Retrieves the column numeric index based on its name.
	 *
	 * @param string $name
	 * @return int|null The corresponding index of the column.
	 */
	protected function getColumnIndexByName($name)
	{
		$index = null;

		foreach ($this->clientOptions['columns'] as $i => $column) {
			if (isset($column['name']) && $column['name'] === $name) {
				$index = $i;
				break;
			}
		}

		return $index;
	}

	/**
	 * Builds a table row with column filters controls.
	 *
	 * @return string
	 */
	protected function buildColumnFiltersRow()
	{
		$cells = array_map(function ($filter) {
			return Html::tag('th', $filter, ['class' => 'filter-column']);
		}, $this->_columnFilters);

		return Json::encode(Html::tag('tr', implode('', $cells), ['class' => 'filters-row']));
	}

	/**
	 * Filters and keeps DataTable valid column attributes.
	 *
	 * @param object $column A DataTable column object.
	 * @return object The filtered DataTable column object.
	 */
	protected function filterColumnAttributes($column)
	{
		$allowedColumnAttributes = get_class_vars(BaseDataTableColumn::class);
		$columnAttributes = get_object_vars($column);

		foreach ($columnAttributes as $attribute => $val) {
			// Remove non DataTable standard attributes
			if (!array_key_exists($attribute, $allowedColumnAttributes)) {
				unset($column->$attribute);
				continue;
			}
			if (is_null($column->$attribute)) {
				unset($column->$attribute);
			}
		}
		unset($column->filter);

		return $column;
	}

	/**
	 * Builds the DataTable lengthMenu plugin option to contain the pageLength option value.
	 *
	 * @param int $pageLength The current pageLength.
	 * @param string $displayAll Indicates if all records can be displayed and sets the proper dropdown option.
	 * @return array
	 */
	public static function buildLengthMenu($pageLength, $displayAll = null)
	{
		if (!$pageLength) {
			$pageLength = 25;
		}
		$lengthMenu = [25, 50, 75, 100];

		if (!in_array($pageLength, $lengthMenu)) {
			$lengthMenu[] = $pageLength;
			sort($lengthMenu);
		}

		$lengthMenu = [
			$lengthMenu,
			$lengthMenu,
		];

		if (!empty($displayAll)) {
			$lengthMenu[0][] = -1;
			$lengthMenu[1][] = $displayAll;
		}

		return $lengthMenu;
	}
}
